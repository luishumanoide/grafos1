/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.PriorityQueue;
import javax.swing.JOptionPane;

/**
 * Clase encargada de la lógica y exploración del arbol del puzzle
 *
 * @author Humanoide
 */
public class Motor {

    static ArrayList<Table> Q;
    static PriorityQueue<Table> P;
    static ArrayList<Table> P1;
    static ArrayList<Table> R;
    HashSet<Table> A;
    String cad;
    int heur = 0;

    public void setHeur(int heur) {
        this.heur = heur;
    }

    /**
     * Iniciar a partir de un arreglo inicial
     *
     * @param ar
     */
    public void initM(int ar[][]) {
        //Q es el conjunto de estados visitados
        Q = new ArrayList<>();
        //P1 son los estados abiertos si usamos busqueda de anchura BFS
        P1 = new ArrayList<>();
        //P son los estados abiertos si usamos heuristica
        P = new PriorityQueue<>();
        //R es para trazar la ruta
        R = new ArrayList<>();
        //A es un hashset para revisar si ya exploramos el elemento
        A = new HashSet<>();
        cad = "";
        Table t = new Table(ar);
        Table fin = new Table(Table.goal);
        Table temp;
        t.setAlt(0);
        Table.setHeur(heur);
        A.add(t);
        //P.push(t);    
        int cnd = 0;
        if (t.solvable()) {
            if (heur == 0) {
                P1.add(t);
                /**
                 * Se hacen 2 whiles para acortar el tiempo de ejecución y hacer
                 * menos comparaciones que si tuviera 1 while
                 */
                while ((P1.size() > 0)) {
                    temp = P1.remove(0);
                    Q.add(temp);
                    if (Q.get(Q.size() - 1).equals(fin)) {
                        findPath();
                        break;
                    }

                    expandB(Q.get(Q.size() - 1));
                    cnd++;
                  
                }
            } else {
                P.add(t);
                while ((P.size() > 0)) {
                    temp = P.poll();

                    Q.add(temp);
                    if (Q.get(Q.size() - 1).equals(fin)) {
                        findPath();
                        break;
                    }

                    expand(Q.get(Q.size() - 1));
                    cnd++;
                }
            }
        } else {
            P1.add(t);
            while ((P1.size() > 0)) {
                temp = P1.remove(0);
                Q.add(temp);
                if (Q.get(Q.size() - 1).equals(fin)) {
                    findPath();
                    break;
                }

                expandB(Q.get(Q.size() - 1));
                cnd++;
                if (cnd > 200000||P1.isEmpty()) {
                            JOptionPane.showMessageDialog(null, "El siguiente puzzle\n No se puede resolver", "Error", JOptionPane.ERROR_MESSAGE);
                            break;
                }
            }

        }
        int cn = 0;
        /**
         * Imprimimos la ruta o el camino para llegar al resultado
         */
        Collections.reverse(R);
        for (Table table : R) {
            cad = cad + "<h2 align=\"center\">Paso " + cn + "</h2>";
            cad = cad + table.printTable();
            cn++;
        }
        cad = cad + "<p align='center'>";
        cad = cad + "|Q|=" + (Q.size());
        if (heur != 0) {
            cad = cad + "   |P|=" + P.size();
        } else {
            cad = cad + "   |P|=" + P1.size();
        }
        cad = cad + "   |A|=" + A.size();
        cad = cad + "</p>";

    }

    public String Process() {
        return cad;
    }

    /**
     * Una vez que encontramos el final, trazamos la ruta y la guardamos en un
     * Array List R
     */
    public void findPath() {
        R.add(Q.get(Q.size() - 1));
        if (Q.get(Q.size() - 1).getAlt() == 0) {
        } else {
            Table t2 = Q.get(Q.size() - 1).getParent();
            while (true) {
                if (t2.getAlt() == 0) {
                    R.add(t2);
                    break;
                }
                R.add(t2);
                t2 = t2.getParent();

            }
        }

    }

    /**
     * Si tenemos una heuristica, expandimos la tabla
     *
     * @param tab La tabla que vamos a expandir
     */
    public void expand(Table tab) {
        ArrayList<Table> childs = new ArrayList();
        int nchilds = 0;
        for (Action act : Action.values()) {
            Table T = new Table(tab.swap(act));
            if (A.add(T)) {
                T.setAlt(tab.getAlt() + 1);
                T.setParent(tab);
                childs.add(T);
                nchilds++;
            }
        }

        if (nchilds > 0) {
            for (Table t : childs) {
                P.add(t);
            }
        }

    }

    /**
     * Si lo hacemos por breadth first search Expandimos de igual manera
     *
     * @param tab tabla que vamos a expandir
     */
    public void expandB(Table tab) {
        ArrayList<Table> childs = new ArrayList();
        int nchilds = 0;
        for (Action act : Action.values()) {
            Table T = new Table(tab.swap(act));
            if (A.add(T)) {
                T.setAlt(tab.getAlt() + 1);
                T.setParent(tab);
                childs.add(T);
                nchilds++;
            }
        }

        if (nchilds > 0) {
            for (Table t : childs) {
                P1.add(t);
            }
        }

    }

}

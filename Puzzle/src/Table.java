/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.Comparator;

/**
 * Clase que representa la tabla del puzzle
 *
 * @author Humanoide
 */
public class Table implements Comparator<Table>, Comparable<Table> {

    int[][] block;
    static int[][] goal;
    int movs;
    int posX, posY;
    int alt;
    static int heur = 0;
    static float alpha;
    ArrayList<Table> childrens = new ArrayList<>();
    Table parent;

    /**
     * el constructor recibe un arreglo 2d
     *
     * @param block bloque de la tabla
     */
    public Table(int[][] block) {
        this.block = block;
        boolean flag = false;
        ///////////////////////////////////
        for (int i = 0; i < size(); i++) {
            for (int j = 0; j < size(); j++) {
                if (block[i][j] == 0) {
                    flag = true;
                    posX = j;
                    posY = i;
                    break;
                }

            }
            if (flag) {
                break;
            }
        }
    }

    /**
     * @return Devuelve cual es el padre de la tabla
     */
    public Table getParent() {
        return parent;
    }

    /**
     * Configuramos el padre de la tabla
     *
     * @param parent
     */
    public void setParent(Table parent) {
        this.parent = parent;
    }

    /**
     * Le damos una altura
     *
     * @param alt
     */
    public void setAlt(int alt) {
        this.alt = alt;
    }

    /**
     * @return devuelve la altura del elemento
     */
    public int getAlt() {
        return alt;
    }

    /**
     *
     * @return devuelve el bloque
     */
    public int[][] getBlock() {
        return block;
    }

    /**
     * Configuramos en todas los objetos tabla el valor del array 2D al que
     * queremos llegar
     *
     * @param goal Un arreglo 2D
     */
    public static void setGoal(int[][] goal) {
        Table.goal = goal;
    }

    /**
     * Imprimimos el elemento
     *
     * @return la tabla del elemento
     */
    public String printTable() {
        String cad = "";
        cad = cad + "<table style=\"width:30%\" border=\"1\" align=\"center\">";
        for (int i = 0; i < size(); i++) {
            cad = cad + "<tr>";

            for (int j = 0; j < size(); j++) {
                if (block[i][j] == 0) {
                    cad = cad + "<td><b><font color=\"blue\">" + block[i][j] + "</font></b></td>";
                } else {
                    cad = cad + "<td><font color=\"black\">" + block[i][j] + "</font></td>";
                }
            }
            cad = cad + "</tr>";
        }
        cad = cad + "</table><br><br></font>";
        return cad;
    }
    
    @Override
    public String toString() {
        return printTable();
    }
    /**
     * Tamaño del array
     * @return 
     */
    public int size() {
        return block[0].length;
    }
    
    public void setMovs(int movs) {
        this.movs = movs;
    }

    public int getMovs(int movs) {
        return movs;
    }

    /**
     * HAMING VALUE FUNCTION
     *
     * @param goal that is an 2D array
     * @return the Hamilton Value
     */
    public int hamil(int[][] goal) {
        int ham = 0;
        for (int i = 0; i < goal[0].length; i++) {
            for (int j = 0; j < goal[0].length; j++) {
                if (block[i][j] != goal[i][j] && block[i][j] != 0) {
                    ham++;
                }
            }
        }
        return ham;
    }

    /**
     * retorna manhatan y se necesita un array 2d
     *
     * @param goal
     * @return
     */
    public int man(int[][] goal) {
        int cm = 0;
        for (int i = 0; i < goal[0].length; i++) {
            for (int j = 0; j < goal[0].length; j++) {
                for (int k = 0; k < goal[0].length; k++) {
                    boolean flag = false;
                    for (int l = 0; l < goal[0].length; l++) {
                        if (goal[i][j] == block[k][l] && goal[i][j] != 0) {
                            cm = cm + Math.abs(k - i) + Math.abs(l - j);
                            flag = true;
                        }
                        if (flag) {
                            break;
                        }
                    }
                }
            }
        }
        return cm;
    }

    /**
     * Hacemos un dezplazamiento en la tabla
     * @param action
     * @return un array 2D del elemento ya dezplazado
     */
    public int[][] swap(Action action) {

        int[][] block2 = new int[block.length][block.length];
        for (int i = 0; i < block.length; i++) {
            System.arraycopy(block[i], 0, block2[i], 0, block.length);
        }
        int size = size();
        switch (action) {
            case DOWN://DOWN
                if ((posY + 1) < size) {
                    int tmp = block2[posY][posX];
                    block2[posY][posX] = block2[posY + 1][posX];
                    block2[posY + 1][posX] = tmp;
                }
                break;
            case LEFT:
                if ((posX - 1) >= 0) {
                    int tmp = block2[posY][posX];
                    block2[posY][posX] = block2[posY][posX - 1];
                    block2[posY][posX - 1] = tmp;
                }
                break;
            case RIGHT:
                if ((posX + 1) < size) {
                    int tmp = block2[posY][posX];
                    block2[posY][posX] = block2[posY][posX + 1];
                    block2[posY][posX + 1] = tmp;
                }
                break;
            case UP:
                if ((posY - 1) >= 0) {
                    int tmp = block2[posY][posX];
                    block2[posY][posX] = block2[posY - 1][posX];
                    block2[posY - 1][posX] = tmp;
                }
                break;
            default:
                break;
        }
        return block2;
    }
    /**
     * Hash unico del elemento
     * @return 
     */
    @Override
    public int hashCode() {
        /**
         * Convertimos el array 2D en uno lineal
         */
        int Array[]=new int[block[0].length*block[0].length];
        int cont=0;
        for (int i = 0; i < block[0].length; i++) {
            for (int j = 0; j < block[0].length; j++) {
                Array[cont] =block[i][j]*cont;
                cont++;
            }
        }
        return java.util.Arrays.hashCode(Array);
    }
    /**
     * Funcion para comparar si son iguales, sirve para insertar en la tabla hash
     * @param o
     * @return un booleano
     */
    @Override
    public boolean equals(Object o) {
        return (o instanceof Table) && (((Table) o).hashCode() == this.hashCode());
    }
    /**
     * Configuramos la heuristica a utilizar
     * @param heur 0=No heuristica, 1=Manhatan, 2=Haming
     */
    public static void setHeur(int heur) {
        Table.heur = heur;
    }
    /**
     * 
     * @return regresamos el valor de la prioridad    /**
     * 
     * @return regresamos el valor de la prioridad
     */
    
    public static void setAlpha(float alpha){
        if(alpha==0.5){
            Table.alpha=(float) 0.51;
        }
        else{
            Table.alpha=alpha; 
        }

    }
    
    public int getPrior() {
        if (heur == 2) {
            return (int)((((1-alpha)*(hamil(Table.goal)))) + (alpha)*alt);
        } else {
            return (int) ((((1-alpha)*(man(Table.goal)))) + (alpha)*alt);
        }

    }
    /**
     * Usado para comparar
     * @param t
     * @param t1
     * @return 
     */
    @Override
    public int compare(Table t, Table t1) {
        return -t.getPrior() + t1.getPrior();
    }

    @Override
    public int compareTo(Table o) {
        return +this.getPrior() - o.getPrior();
    }
    
    
    /* Se usa para calcular el valor de los inversos*/
    public int inverse(){
        int[] temp = new int[block.length*block.length+1];
        int cont = 0;
        /*Se genera un arreglo con los valores del tablero ordenados deacuerdo
        * al objetivo */
        for (int i = 0; i < size(); i++) {
            for (int j = 0; j < size(); j++) {
                temp[goal[i][j]]=block[i][j];
                System.out.println(goal[i][j]);
                if(goal[i][j]==0)
                    temp[size()*size()]=block[i][j];
            }             
        }
        //Se cuentan los inversos
        for (int i = 1; i < size()*size()+1; i++) {
            for (int j = i+1; j < size()*size()+1; j++) {
                if (temp[i] > temp[j]&&temp[i]!=0&&temp[j]!=0) {
                    cont++;
                }
            }            
        }
        return cont;
    }
    
    /*Se usa para saber si el puzzle se puede resolver o no*/
    public boolean solvable(){
        return inverse()%2==0;
    }
    

}

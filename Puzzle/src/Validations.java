/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.event.KeyEvent;
import static java.awt.event.KeyEvent.VK_0;
import static java.awt.event.KeyEvent.VK_1;
import static java.awt.event.KeyEvent.VK_2;
import static java.awt.event.KeyEvent.VK_3;
import static java.awt.event.KeyEvent.VK_4;
import static java.awt.event.KeyEvent.VK_5;
import static java.awt.event.KeyEvent.VK_6;
import static java.awt.event.KeyEvent.VK_7;
import static java.awt.event.KeyEvent.VK_8;
import static java.awt.event.KeyEvent.VK_9;
import static java.awt.event.KeyEvent.VK_NUMPAD0;
import static java.awt.event.KeyEvent.VK_NUMPAD1;
import static java.awt.event.KeyEvent.VK_NUMPAD2;
import static java.awt.event.KeyEvent.VK_NUMPAD3;
import static java.awt.event.KeyEvent.VK_NUMPAD4;
import static java.awt.event.KeyEvent.VK_NUMPAD5;
import static java.awt.event.KeyEvent.VK_NUMPAD6;
import static java.awt.event.KeyEvent.VK_NUMPAD7;
import static java.awt.event.KeyEvent.VK_NUMPAD8;
import static java.awt.event.KeyEvent.VK_NUMPAD9;
import javax.swing.JTable;

/**
 *
 * @author Humanoide
 */
public class Validations {
    /**
     * Validamos si esta introduciendo números para
     * @param e
     * @return true si introduce numeros
     */
    static boolean isNumberKey(KeyEvent e){
       int keys[]={VK_NUMPAD0,VK_NUMPAD1,VK_NUMPAD2,VK_NUMPAD3,VK_NUMPAD4,VK_NUMPAD5,VK_NUMPAD6,VK_NUMPAD7,VK_NUMPAD8,VK_NUMPAD9,VK_0,VK_1,VK_2,VK_3,VK_4,VK_5,VK_6,VK_7,VK_8,VK_9};
       for(int key:keys){
           if(e.getKeyCode()==key){
               return true;
           }
       }
       return false;
    }
    /**
     * Validamos que no se repitan elementos
     * @param table
     * @return 
     */
    static boolean noRepeat(JTable table){
        int rows=table.getRowCount();
        int cols=table.getColumnCount();
        System.out.println("--------------"+rows);
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
                if(exploreTable(table,Integer.parseInt(""+ table.getValueAt(i, j)))){
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * Exploramos la tabla en la interdaz
     * @param table
     * @param value
     * @return 
     */
    static boolean exploreTable(JTable table, int value){
        int c=0;
        for(int i=0;i<table.getRowCount();i++){
            for(int j=0;j<table.getColumnCount();j++){
                if(value==Integer.parseInt(""+ table.getValueAt(i, j))){
                    c++;
                }
            }
        }
        return c>1;
    }
    /**
     * Validamos que la tabla esté llena
     * @param table
     * @return true si está llena
     */
    static boolean isFull(JTable table){
        int c=0;
        for(int i=0;i<table.getRowCount();i++){
            for(int j=0;j<table.getColumnCount();j++){
                if(!(""+ table.getValueAt(i, j)).equals("")&&table.getValueAt(i, j)!=null){
                int value=Integer.parseInt(""+ table.getValueAt(i, j));
                    if(value>=0&&value<table.getRowCount()*table.getColumnCount()){
                        c++;
                    }
                }
            }
        }
        return c==table.getRowCount()*table.getRowCount();
    }
    
}
